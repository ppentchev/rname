/*
 * Copyright (c) 2000, 2017 Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include <err.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifndef __unused
#ifdef __GNUC__
#define __unused	__attribute__((unused))
#else
#define __unused
#endif /* __GNUC__ */
#endif /* __unused */

static void	rn_help(void);
static void	rn_version(void);
static void	rn_init(int argc, char * const argv[]);
static void	rn_doit(int argc, char * const argv[]);

/***
Function:
	rn_init				- general-purpose init function
Inputs:
	argc, argv			- main() args for option processing
Returns:
	Nothing; dies on error.
Modifies:
	Nothing.
***/

static void
rn_init(const int argc, char * const argv[]) {
	int ch;
	bool helpq = false, versq = false;

	/* Process cmdline options */

	while (ch = getopt(argc, argv, "hV"), ch != -1)
		switch(ch) {
			case 'h':
				helpq = true;
				break;
			case 'V':
				versq = true;
				break;
			case '?':
			default:
				exit(1);
				/* NOTREACHED */
		}

	if (versq)
		rn_version();
	if (helpq)
		rn_help();
	if (versq || helpq)
		exit(0);

	if (argc < optind + 2) {
		warnx("At least two arguments expected: a fake name and the real program's name");
		rn_help();
		exit(1);
	}
}

/***
Function:
	rn_help					- startup help info
Inputs:
	none
Returns:
	Nothing.
Modifies:
	nothing, writes to stdout
***/

static void
rn_help(void) {
	puts(
	"Usage:\trname fakename realname [args..]\n"
	"\trname -V | -h\n"
	"\n"
	"\t-h\tprint this help text and exit;\n"
	"\t-V\tprint version information and exit."
	);
}

/***
function:
	rn_version				- output version info
Inputs:
	none
Returns:
	Nothing.
Modifies:
	nothing, writes to stdout
***/

static void
rn_version(void) {
	puts("rname 1.0.2");
}

/***
Function:
	rn_doit				- actually execute something
Inputs:
	none
Returns:
	Nothing; dies on error.
Modifies:
	copies argv to a newly-allocated array with the first element replaced,
	then exec's the requested program
***/

static void
rn_doit(const int argc, char * const argv[]) {
	const size_t nargc = argc - 1;
	char ** const nargv = malloc((nargc + 1) * sizeof(*nargv));
	if (nargv == NULL)
		err(1, "Could not allocate memory for the new command line array");
	nargv[0] = argv[0];
	for (size_t i = 1; i < nargc; i++)
		nargv[i] = argv[i + 1];
	nargv[nargc] = NULL;

	execvp(argv[1], nargv);
	err(1, "Could not execute '%s' as '%s'", argv[1], nargv[0]);
}

int
main(const int argc, char * const argv[]) {
	rn_init(argc, argv);
	rn_doit(argc - optind, argv + optind);
	return 0;
}
