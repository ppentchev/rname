#!/bin/sh
#
# Copyright (c) 2000, 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

BDECFLAGS=	-W -Wall -Wbad-function-cast -Wcast-align \
		-Wcast-qual -Wchar-subscripts -Wconversion -Wno-sign-conversion -Winline \
		-Wmissing-prototypes -Wnested-externs -Wpointer-arith \
		-Wredundant-decls -Wshadow -Wstrict-prototypes -Wwrite-strings

CC		?= cc

CPPFLAGS_STD	?= -D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700

CFLAGS_WARN	?= -Wall -W -Wstrict-prototypes -Wmissing-prototypes -Wwrite-strings ${BDECFLAGS}
CFLAGS_OPT	?=
#CFLAGS_OPT	?= -O2 -pipe
CFLAGS_DBG	?= -ggdb -g3
#CFLAGS_DBG	?=
CFLAGS_STD	?= -std=c99 -pedantic

CPPFLAGS	?=
CPPFLAGS	+= ${CPPFLAGS_STD}

CFLAGS		?= ${CFLAGS_WARN} ${CFLAGS_OPT} ${CFLAGS_DBG}
CFLAGS		+= ${CFLAGS_STD}

LDFLAGS		?=

RM		?= rm -f
MKDIR		?= mkdir -p
ECHO		?= echo

INSTALL		?= /usr/bin/install

BINOWN		?= root
BINGRP		?= root
BINMODE		?= 555

SHAREOWN	?= ${BINOWN}
SHAREGRP	?= ${BINGRP}
SHAREMODE	?= 444

INST_COPY	?= -c
INST_MODE_P	?= -m 755
INST_MODE_D	?= -m 644
INST_OWN_P	?= -o root
INST_OWN_D	?= -o root

INSTALL_PROGRAM	?= ${INSTALL} ${INST_COPY} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA	?= ${INSTALL} ${INST_COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

PREFIX		?= /usr/local
BINDIR		?= ${PREFIX}/bin
MANDIR		?= ${PREFIX}/share/man
MAN1DIR		?= ${MANDIR}/man1

TGT		= rname
OBJS		= rname.o
SRCS		= rname.c
LIBS		=
MAN1		= ${TGT}.1
MAN1Z		= ${MAN1}.gz

all:		${TGT} ${MAN1Z}

clean:
		${RM} ${TGT} ${OBJS} ${MAN1Z}

install:	install-bin install-man

install-bin:	${TGT}
		${MKDIR} ${DESTDIR}${BINDIR}
		${INSTALL_PROGRAM} ${TGT} ${DESTDIR}${BINDIR}

install-man:	${MAN1Z}
		${MKDIR} ${DESTDIR}${MAN1DIR}
		${INSTALL_PROGRAM} ${MAN1Z} ${DESTDIR}${MAN1DIR}

install-share:

${TGT}:		${OBJS}
		${CC} ${LDFLAGS} -o ${TGT} ${OBJS} ${LIBS}

.c.o:
		${CC} -c ${CPPFLAGS} ${CFLAGS} $<

%.1.gz:		%.1
		gzip -nc $< > $@ || (rm -f -- $@; false)

test:		all
		prove t
