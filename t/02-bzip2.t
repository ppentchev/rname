#!/bin/sh
#
# Copyright (c) 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

if [ -f 'tap-functions.sh' ]; then
	. tap-functions.sh
elif [ -f 't/tap-functions.sh' ]; then
	. t/tap-functions.sh
else
	echo 'Bail out! Could not find tap-functions.sh'
	exit 99
fi

echo '# creating three temporary files'
tempf="$(mktemp 'test-output.txt.XXXXXX')"
trap "rm -f '$tempf'" EXIT QUIT TERM INT HUP 
temphelp="$(mktemp 'test-help.txt.XXXXXX')"
trap "rm -f '$tempf' '$temphelp'" EXIT QUIT TERM INT HUP 
temperrs="$(mktemp 'test-errors.txt.XXXXXX')"
trap "rm -f '$tempf' '$temphelp' '$temperrs'" EXIT QUIT TERM INT HUP 
echo "# created temporary files: output: $tempf; help: $temphelp"

[ -z "$RNAME" ] && RNAME='./rname'

plan_ 22

echo '# trying to run a nonexistent program should fail'
$RNAME something no-such-program-we-hope > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 1 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ ! -s "$tempf" ]; then ok_; else not_ok_ 'output with a nonexistent program'; fi
if [ -s "$temperrs" ]; then ok_; else not_ok_ 'no error output with a nonexistent program'; fi

echo '# trying to run "true" should work'
$RNAME something true > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ ! -s "$tempf" ]; then ok_; else not_ok_ 'output with "true"'; fi
if [ ! -s "$temperrs" ]; then ok_; else not_ok_ 'no error output with "true"'; fi

echo '# trying to run "false" should fail'
$RNAME something false > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" != 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ ! -s "$tempf" ]; then ok_; else not_ok_ 'output with "false"'; fi
if [ ! -s "$temperrs" ]; then ok_; else not_ok_ 'no error output with "false"'; fi

echo '# check if bzip2 may be invoked the way we like it'
bzip2 -h > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ ! -s "$tempf" ]; then ok_; else not_ok_ 'bzip2 -h produced output'; fi
if [ -s "$temperrs" ]; then ok_; else not_ok_ 'bzip2 -h did not produce error output'; fi
if fgrep -qe 'usage: bzip2' -- "$temperrs"; then ok_; else not_ok_ 'bzip2 -h did not produce "usage: bzip2"'; fi

echo '# OK, now run bzip2 as bunzip2'
$RNAME bunzip2 bzip2 -h > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ ! -s "$tempf" ]; then ok_; else not_ok_ 'bzip2 -h produced output'; fi
if [ -s "$temperrs" ]; then ok_; else not_ok_ 'bzip2 -h did not produce error output'; fi
if fgrep -qe 'usage: bunzip2' -- "$temperrs"; then ok_; else not_ok_ 'rname bunzip2 bzip2 -h did not produce "usage: bunzip2"'; fi

echo '# Right, now actually compress and uncompress something'
tline='This is a test'
echo "$tline" | bzip2 -c | $RNAME bunzip2 bzip2 -c > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ -s "$tempf" ]; then ok_; else not_ok_ 'bzip2 | bunzip2 did not produce output'; fi
if [ ! -s "$temperrs" ]; then ok_; else not_ok_ 'bzip2 | bunzip2 produced error output'; fi
if [ "$(wc -l < "$tempf")" -eq 1 ]; then ok_; else not_ok_ 'bzip2 | bunzip2 did not produce a single line'; fi
nline="$(cat -- "$tempf")"
if [ "$nline" = "$tline" ]; then ok_; else not_ok_ 'bzip2 | bunzip2 did not produce the original line'; fi
