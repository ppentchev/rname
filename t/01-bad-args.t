#!/bin/sh
#
# Copyright (c) 2017  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

if [ -f 'tap-functions.sh' ]; then
	. tap-functions.sh
elif [ -f 't/tap-functions.sh' ]; then
	. t/tap-functions.sh
else
	echo 'Bail out! Could not find tap-functions.sh'
	exit 99
fi

echo '# creating three temporary files'
tempf="$(mktemp 'test-output.txt.XXXXXX')"
trap "rm -f '$tempf'" EXIT QUIT TERM INT HUP 
temphelp="$(mktemp 'test-help.txt.XXXXXX')"
trap "rm -f '$tempf' '$temphelp'" EXIT QUIT TERM INT HUP 
temperrs="$(mktemp 'test-errors.txt.XXXXXX')"
trap "rm -f '$tempf' '$temphelp' '$temperrs'" EXIT QUIT TERM INT HUP 
echo "# created temporary files: output: $tempf; help: $temphelp"

[ -z "$RNAME" ] && RNAME='./rname'

plan_ 18

echo '# rname with no arguments should exit with code 1'
$RNAME > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 1 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ -s "$tempf" ]; then ok_; else not_ok_ 'no standard output with no arguments'; fi
if [ -s "$temperrs" ]; then ok_; else not_ok_ 'no error output with no arguments'; fi

echo '# get the single-line version output'
$RNAME -V > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$(wc -l < "$tempf")" = 1 ]; then ok_; else not_ok_ 'more than one version line'; fi
if [ ! -s "$temperrs" ]; then ok_; else not_ok_ 'error output with -V'; fi
vline="$(cat -- "$tempf")"
echo "# got $vline"

echo '# get the multi-line help output'
$RNAME -h > "$temphelp" 2>"$temperrs"
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ "$(wc -l < "$temphelp")" -ge 2 ]; then ok_; else not_ok_ 'fewer than two help lines'; fi
if [ ! -s "$temperrs" ]; then ok_; else not_ok_ 'error output with -h'; fi

echo '# now try for both help and version'
$RNAME -V -h > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if (echo "$vline"; cat -- "$temphelp") | diff -q -- - "$tempf" > /dev/null 2>&1; then ok_; else not_ok_ 'rname -V -h produced unexpected output'; fi
if [ ! -s "$temperrs" ]; then ok_; else not_ok_ 'error output with -V -h'; fi

$RNAME -hV > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 0 ]; then ok_; else not_ok_ "exit code $res"; fi
if (echo "$vline"; cat -- "$temphelp") | diff -q -- - "$tempf" > /dev/null 2>&1; then ok_; else not_ok_ 'rname -hV produced unexpected output'; fi
if [ ! -s "$temperrs" ]; then ok_; else not_ok_ 'error output with -hV'; fi

echo '# fail with unknown option arguments'
$RNAME -a foo true > "$tempf" 2>"$temperrs"
res="$?"
if [ "$res" = 1 ]; then ok_; else not_ok_ "exit code $res"; fi
if [ ! -s "$tempf" ]; then ok_; else not_ok_ 'output with invalid options'; fi
if [ -s "$temperrs" ]; then ok_; else not_ok_ 'no error output with invalid options'; fi
